using JLD2
using FileIO

using PyCall

@pyimport scipy.io as mio

function load_geometry_mat(name)
    #creates a dictionary that mapps varNames to content
    d=mio.loadmat(name)
    
    #edges are loaded as an Float64-Array. propably because of floatingpoint-values in 
    #lines 3-4. I flatten all floating-points and cast the whole array to Int, since 
    #we dont need the lines 3,4 in this exercise
    edges=Int.(floor.(d["edges"]))
    points=d["points"]
    #for some reason triangles too...
    triangles=Int.(d["triangles"])
    (points,edges,triangles)
end

#= 3 Reasons for this function instead of @load-macro:
1. I get to name the variables myself
2. Automatic Int-conversion for edges (see below)
3. the macro requires a String-literal at compiletime. this function takes any String at runtime
=#
function load_geometry(name)
    #creates a dictionary that mapps varNames to content
    d=load(name)
    
    #edges are loaded as an Float64-Array. propably because of floatingpoint-values in 
    #lines 3-4. I flatten all floating-points and cast the whole array to Int, since 
    #we dont need the lines 3,4 in this exercise
    edges=Int.(floor.(d["edges"]))
    points=d["points"]
    #for some reason triangles too...
    triangles=Int.(d["triangles"])
    (points,edges,triangles)
end

#we need this later
#transforms a multidimensional array into a 1d-Array
function flatten(S)
    dim=1
    for d in size(S)
        dim*=d
    end
    reshape(S,dim)
end


function  compress_nodes_to_dof(points, edges, neumann_segments=[])
    ## VORGEGEBEN
    # Da wir, wie in der Vorlesung vorgestellt, bei
    # Dirichlet-Randbedingungen nur mit den inneren Knoten arbeiten,
    # müssen wir zunächst bestimmen, welche Knoten der Triangulation (hier:
    # nodes) im Inneren liegen und damit aals Variablen im Gleichungssystem
    # (hier: dof, degrees of freedom) auftauchen.
    #
    # Eingabe:
    # . points.  
    # . edges.  
    # . neumann_segments:  Liste der Segment-Indizes, die einem
    #       Neumann-Rand entsprechen. Diese müssen wie innere Knoten
    #       behandelt werden.
    #
    # Rückgabe:
    # . node2dof:  Vektor, der jedem node index einen dof (degree of freedom) 
    #       index zuordnet.
    # . dof2node:  Vektor, der jedem dof index den entsprechenden node
    #       index der Triangulation zuordnet.
    # . ndof:  Anzahl der dof (also Variablen im Gleichungssystem)
    # . node2seg:  Speichert zu jedem node, wenn dieser auf dem Rand
    #       liegt, den Index des Randsegments ab.
    
    # Wenn keine Neumann-Rand-Segment-Indizes übergeben wurden, haben wir
    # nur Dirichlet-Rand.
    # ??? Error 404: Code not found ???
    # Hilfsvariablen
    npoints = size(points, 2);
    nedges  = size(edges, 2);
    
    # Passende Vektoren anlegen
    node2dof = zeros(Int64,npoints, 1);
    dof2node = zeros(Int64,npoints, 1);
    node2seg = zeros(Int64,npoints, 1);
    
    # Speichert zu jedem Knoten, ob dieser auf dem Rand des Gebiets liegt.
    is_node_on_border = fill(false,size(points, 2), 1);
    
    # Iteriere über jede Kante und prüfe, ob es sich um eine Kante auf dem
    # Rand des Gebiets handelt.
    for i in 1:nedges
        if (edges[6, i] == 0 || edges[7, i] == 0)
            # Falls ja, prüfe, ob es sich dabei um eine Kante auf dem
            # Dirichlet-Rand handelt.
            if !( edges[5, i] in neumann_segments)
                # Falls ja, markiere diese Kante als "auf dem Rand"
                is_node_on_border[edges[1, i]] = true;
                is_node_on_border[edges[2, i]] = true;
                # Und speichere in node2seg den Index des Rand-Segments ab. 
                # Dies ist nützlich, wenn inhomogener Dirichlet-Rand vorliegt.
                node2seg[edges[1, i]] = edges[5, i];
                node2seg[edges[2, i]] = edges[5, i];
            end
        end
    end
    
    # Iteriere jetzt über alle Knoten und prüfe, ob diese als "auf dem
    # Rand" markiert wurden. Falls nicht, dann handelt es sich um einen
    # inneren node, d.h. einen dof. Dieser bekommt einen aufsteigenden
    # Index iter_dof, der in node2dof und dof2node vermerkt wird.
    iter_dof = 1;
    for i in 1:npoints
        if !is_node_on_border[i]
            node2dof[i]        = iter_dof;
            dof2node[iter_dof] = i;
            iter_dof           = iter_dof + 1;
        end
    end
    
    # Bestimme die Anzahl der dof und kürze dof2node auf diese Anzahl.
    ndof     = iter_dof - 1;
    dof2node = dof2node[1:ndof];
    
    (node2dof, dof2node, ndof, node2seg)
end


