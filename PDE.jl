eye=I


function plotnow(points, triangles, uk,name="")
    ## VORGEGEBEN
    # Plotte das Ergebnis des aktuellen Iterationsschritts.
    # 
    # Eingabe:
    # . points.
    # . triangles.
    # . uk:
    #       Ergebnis der PDE für den festen Zeitpunkt tk mit eingebauten 
    #       Randbedingungen (d.h. mit Werten für alle Knoten der Triangulation)
    #       als Vektor.
    # . tk:
    #       Zeitpunkt, für den uk die Lösung ist.


    
    
    fig=plt.figure()
    ax=fig[:gca](projection="3d")
    tri=Array(triangles[1:3,:]').-1

    ax[:plot_trisurf](points[1,:],points[2,:],uk,triangles=tri)

  
    
    
    ax[:set_xlabel]("X");  ax[:set_ylabel]("Y");  ax[:set_zlabel]("Z")
    ax[:set_title](name);
    plt.tight_layout()

    plt.show()
end





function assemble_element_rhs(coords, fhandle)
    ## VORGEGEBEN
    # Assembliert den Elementlastvektor mittels numerischer Quadratur.
    #
    # Eingabe:
    # . coords:
    #       Vektor der Koordinaten der drei Eckpunkte eines Dreiecks
    # . fhandle:
    #       Funktion der rechten Seite als function_handle (*darf* nur von 
    #       Ortsvariable abhängig sein!)
    #
    # Erwartete Rückgabe:
    # . fT:
    #       Vektor der rechten Seite auf dem Dreieck (3x1)

    # Extrahiere Knoten des Dreiecks
    x     = coords[:, 1]; y = coords[:, 2]; z = coords[:, 3];
    # Determinante der Ableitung der Transformationsabb. berechnen
    detFT = (y[1] - x[1]) * (z[2] - x[2]) - (y[2] - x[2]) * (z[1] - x[1]);
    # ... und die numerische Quadratur aufrufen.
    detFT * num_quad_tridiag_5(coords, fhandle);    
end

function  num_quad_tridiag_5(coords, fhandle)
    ## VORGEGEBEN
    # Numerische Quadratur auf dem Referenzdreieck , bekannt aus Prog. 07
    #
    # Eingaben:
    # . coords:
    #       Koordinaten der drei Eckpunkte des Dreiecks.
    # . fhandle:
    #       function_handle der zu integrierenden Funktion der rechten Seite
    #       (die Basisfunktionen sind nicht Teil des function_handles
    #       und müssen dementsprechend in dieser Quadratur berücksichtigt
    #       werden.)
    #
    # Erwartete Rückgabe:
    # . ivals:
    #       (3 x 1)-Vektor, numerische Approximation des Integralwertes von
    #       f * phi_i auf dem Element, i = 1, 2, 3, mit phi_i nodale
    #       Hutfunktionen des Dreiecks.

    # Knotenkoordinaten auf dem Referenzdreieck ...
    Zs = [(6 - sqrt(15)) / 21; (9 + 2*sqrt(15)) / 21; (6 - sqrt(15)) / 21; 
        (6 + sqrt(15)) / 21; (6 + sqrt(15)) / 21; (9 - 2*sqrt(15)) / 21; 1 / 3];
    Zt = [(6 - sqrt(15)) / 21; (6 - sqrt(15)) / 21; (9 + 2*sqrt(15)) / 21; 
        (9 - 2*sqrt(15)) / 21;  (6 + sqrt(15)) / 21; (6 + sqrt(15)) / 21; 1 / 3];
    # ... und passende Gewichte
    W = [(155 - sqrt(15)) / 2400; (155 - sqrt(15)) / 2400; (155 - sqrt(15)) / 2400; 
            (155 + sqrt(15)) / 2400; (155 + sqrt(15)) / 2400; 
            (155 + sqrt(15)) / 2400; 9 / 80];

    # Koordinaten des Dreiecks extrahieren
    x = coords[:, 1]; y = coords[:, 2]; z = coords[:, 3];

    # numerische Quadratur ausführen
    #???
    ivals = zeros(3);
    for i in 1:7
        # X entspricht den Koordinaten auf dem Element (nicht Referenzdreieck!;
        # d.h. Quadraturknoten des Referenzdreiecks transformiert auf Element)
        X   = x + Zs[i] * (y - x) + Zt[i] * (z - x);
        # Basisfunktionen auswerten
        Z   = [1 - Zs[i] - Zt[i]; Zs[i]; Zt[i]];
        # nächsten "Punkt" der Quadraturformel mit passendem Gewicht addieren
        ivals = ivals + W[i] * fhandle(X) * Z;
    end
    ivals
end



function  assemble_global_rhs(fhandle, t, C, points, triangles, meshdata)
    ## VORGEGEBEN
    # Assembliert die globale rechte Seite für die dofs, d.h. die inneren Knoten 
    # der Triangulation.
    #
    # Eingabe:
    # . fhandle:
    #       function_handle für die rechte Seite der PDE
    # . t: 
    #       Zeitpunkt, für den fhandle ausgewertet werden soll.
    # . C:
    #       Diffusionskoeffizienten für die Subdomains (hier: als 3x1 Vektor)
    # . points:
    #       Punkte der Triangulation
    # . triangles:
    #       Dreiecke der Triangulation
    # . meshdata:
    #       Hilfsdaten der verwendeten Ortsdiskretisierung
    #       
    # Erwartete Rückgabe:
    # . f:
    #       rechte-Seite-Vektor

    # Extrahieren einiger benötigter Größen
    ndof    = size(meshdata[:dof2node], 1);   # Anzahl degrees of freedom
    nt      = size(triangles, 2);           # Anzahl Dreiecke in der Triangulation
    f       = zeros(ndof, 1);               # leeren rechte Seite Vektor anlegen

    # Datenstrukturen für den Aufbau der Sparse-Matrix
    iter = 0;

    # Matrix / Lastvektor assemblieren
    for i in 1:nt
        tri    = triangles[1:3, i];
        coeff  = C[triangles[4, i]];
        coords = points[:, tri];
        # Element-Lastvektor für festen Zeitpunkt t auswerten
        fk     = assemble_element_rhs(coords, X -> fhandle(t, X));

        # Randbedingungen berücksichtigen.
        to_remove = fill(false,3);
        for j in 1:3
            #TODO does this actually do anything?
            if meshdata[:node2seg][tri[j]] in  meshdata[:seg_neumann]
                # node liegt auf homogenem Neumann-Rand, d.h. wird wie ein innerer
                # Knoten behandelt.
                continue;
            elseif meshdata[:node2seg][tri[j]] in meshdata[:seg_dirichlet]
                # node liegt auf inhomogenem Dirichlet-Rand, d.h. Wert der Lösung 
                # ist vorgegeben und geht damit in die rechte Seite ein.
                (Sk, Mk) = assemble_element_stiffness_mass(coords);
                Sk       = coeff * Sk;
                fk       = fk - Sk[:, j] * inh_dirichlet_rb(t, points[:, tri[j]]);
                to_remove[j] = true;
            end
        end

        # Entferne nicht-dofs aus rechter Seite
        fk = fk[.!to_remove];

        # Entferne nicht-dofs auch aus Knotenmenge des Dreiecks und
        # bestimme die dof indices der verbleibenden nodes des Dreiecks
        tri2dof = meshdata[:node2dof][tri[.!to_remove]];

        # rechte Seite updaten
        f[tri2dof] = f[tri2dof] + fk;
    end
    f
end



function assemble_global_stiffness_mass(C, points, triangles, meshdata)
    ## VORGEGEBEN
    # Assembliert die globale Steifigkeitsmatrix und die globale Massematrix. 
    # Diese sind bei dieser Aufgabe zeitunabhängig, müssen also nicht für jeden 
    # Zeitschritt neu bestimmt werden!
    #
    # Eingabe:
    # . C:
    #       Diffusionskoeffizienten für die Subdomains (hier: als 3x1 Vektor)
    # . points:
    #       Punkte der Triangulation
    # . triangles:
    #       Dreiecke der Triangulation
    # . meshdata:
    #       Hilfsdaten der verwendeten Ortsdiskretisierung
    #       
    # Erwartete Rückgabe:
    # . S:
    #       Steifigkeitsmatrix
    # . M:
    #       Massematrix

    # Extrahieren einiger benötigter Größen
    ndof    = size(meshdata[:dof2node], 1);   # Anzahl degrees of freedom
    nt      = size(triangles, 2);           # Anzahl Dreiecke in der Triangulation
    max_nnz = nt * 9;                       # obere Schranke Anzahl nicht-Null Einträge

    # Datenstrukturen für den Aufbau der Sparse-Matrizen
    idx   = zeros(max_nnz, 1);
    idy   = zeros(max_nnz, 1);
    svl_S = zeros(max_nnz, 1);
    svl_M = zeros(max_nnz, 1);
    iter  = 0;

    # Matrizen assemblieren
    for i in 1:nt
        tri      = triangles[1:3, i];
        coeff    = C[triangles[4, i]];
        coords   = points[:, tri];
        (Sk, Mk) = assemble_element_stiffness_mass(coords);

        # Diffusionskoeffizient berücksichtigen
        Sk       = coeff * Sk;

        # Randbedingungen berücksichtigen
        to_remove = fill(false,3);
        for j in 1:3
            if meshdata[:node2seg][tri[j]] in meshdata[:seg_neumann]
                # node liegt auf homogenem Neumann-Rand, Knoten wird als innerer
                # Knoten behandelt.
                continue;
            elseif meshdata[:node2seg][tri[j]] in meshdata[:seg_dirichlet]
                # node liegt auf inhomogenem Dirichlet-Rand, Knoten wird nicht 
                # als innerer Knoten behandelt.
                to_remove[j] = true;
            end
        end
        
        
        to_keep = .!to_remove
        
        # Entferne nicht-dofs aus Elementsteifigkeitsmatrix / -massematrix
        Sk = Sk[to_keep, to_keep];
        Mk = Mk[to_keep, to_keep];

        # Hilfsgrössen zur korrekten Assemblierung der sparse-Daten
        m  = size(Sk, 1);
        m2 = m * m;

        # in Vektor-form transformieren
        Sk = flatten(Sk);
        Mk = flatten(Mk);

        # Entferne nicht-dofs auch aus Knotenmenge des Dreiecks und
        # bestimme die dof indices der verbleibenden nodes des Dreiecks
        tri2dof = meshdata[:node2dof][tri[to_keep]];

        # Werte der Elementmatrizen in sparse-Daten übernehmen
        idx[iter + 1: iter + m2]   = kron(tri2dof, ones(m, 1));
        idy[iter + 1: iter + m2]   = kron(ones(m, 1), tri2dof);
        svl_S[iter + 1: iter + m2] = Sk;
        svl_M[iter + 1: iter + m2] = Mk;
        iter                       = iter + m2;
    end

    # Sparse-Matrizen erstellen
    S = sparse(idx[1:iter], idy[1:iter], svl_S[1:iter], ndof, ndof);
    M = sparse(idx[1:iter], idy[1:iter], svl_M[1:iter], ndof, ndof);
    
    (S,M)
end




function inh_dirichlet_rb(t, X)
    ## VORGEGEBEN
    # Inhomogene Dirichlet-Randbedingung g(t, X) am unteren Rand.
    # 
    # Eingabe:
    # . t:
    #       Zeitvariable
    # . X:
    #       2d-Ortsvariable (d.h. 2x1-Vektor)       
    #       
    # Rückgabe:
    # . z:
    #       Auswertung der inhomogenen Dirichlet-Randbedingung

    # Ortsunabh. An- und Abstieg der Temperatur in Abh. der Zeit
     2 * t * (1 - t);
end

function  source_handle(t, X)
    ## VORGEGEBEN
    # Quellterm als function_handle wie auf dem Üblatt angegeben.
    # 
    # Eingabe:
    # . t:
    #       Zeitvariable
    # . X:
    #       2d-Ortsvariable (d.h. 2x1-Vektor)
    # 
    # Rückgabe:
    # . z:
    #       Auswertung der Quelltermfunktion für (t, X)

    # Quellterm lässt sich als Tensorprodukt einer rein-zeitabh. und 
    # rein-ortsabh. Funktion schreiben:
    
    # Zeitlicher Anteil
    phi_t = 0;
    if 0.25 <= t && t <= 0.5
        phi_t = (t - 0.25) / (0.25);
    elseif 0.5 < t
        phi_t = 1;
    end

    # Ortsanteil
    psi_x = 0;
    if 0.25 <= X[1] && X[1] <= 0.75 && 0.25 <= X[2] && X[2] <= 0.75
        psi_x = 1;
    end
    
    # Kombination und Skalierung
     -10 * phi_t * psi_x;
end

function assemble_element_stiffness_mass(coords)
    ## VORGEGEBEN
    # Assembliert die Elementsteifigkeitsmatrix und die Elementmassematrix.
    #
    # Eingabe:
    # . coords:
    #       Vektor der Koordinaten der drei Eckpunkte eines Dreiecks
    #
    # Erwartete Rückgabe:
    # . ST:
    #       Elementsteifigkeitsmatrix (3x3)
    # . MT:
    #       Elementmassematrix (3x3)

    # Extrahiere Knoten des Dreiecks
    x     = coords[:, 1]; y = coords[:, 2]; z = coords[:, 3];
    # Determinante der Ableitung der Trafo.abb.
    detFT = (y[1] - x[1]) * (z[2] - x[2]) - (y[2] - x[2]) * (z[1] - x[1]);
    # Ableitung der Trafo. von gegebenem Dreieck auf Referenzdreiec
    iFT   = [z[2]-x[2] x[1]-z[1]; x[2]-y[2] y[1]-x[1]] / detFT;
    # Gradienten der Hutfunktionen auf dem Referenzdreieck
    D     = [-1 -1; 1 0; 0 1];
    # Element-Steifigkeitsmatrix ...
    ST    = detFT * (D * (iFT * iFT') * D') / 2;
    # ... und Element-Massematrix bestimmen
    MT    = detFT * (ones(3, 3) + eye) / 24;
    (ST,MT)
end
